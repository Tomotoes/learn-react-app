export default [
	{
		id: 'Introduction',
		route: '/tutorial/react-introduction',
		displayName: '0 - React 简介',
		markdownLocation: 'src/tutorial/build/00-ReactIntroduction.js'
	},
	{
		id: 'HelloWorld',
		route: '/tutorial/hello-world',
		displayName: '1 - Hello World',
		markdownLocation: 'src/tutorial/build/01-HelloWorld.js',
		exercises: [
			{
				tag: 'exercise1',
				location: 'src/exercise/01-HelloWorld.js',
				solutionLocation: 'src/exercise/solution/01-HelloWorld-solution.js'
			}
		]
	},
	{
		id: 'IntroToJSX',
		route: '/tutorial/intro-to-jsx',
		displayName: '2 - 认识 JSX',
		markdownLocation: 'src/tutorial/build/02-IntroToJSX.js',
		exercises: [
			{
				tag: 'exercise1',
				location: 'src/exercise/02-IntroToJSX.js',
				solutionLocation: 'src/exercise/solution/02-IntroToJSX-solution.js'
			}
		]
	},
	{
		id: 'PowerOfJSX',
		route: '/tutorial/power-of-jsx',
		displayName: '3 - JSX 的优势',
		markdownLocation: 'src/tutorial/build/03-PowerOfJSX.js',
		exercises: [
			{
				tag: 'exercise1',
				location: 'src/exercise/03-PowerOfJSX.js',
				solutionLocation: 'src/exercise/solution/03-PowerOfJSX-solution.js'
			}
		]
	},
	{
		id: 'Props',
		route: '/tutorial/understanding-props',
		displayName: '4 - 掌握 Props',
		markdownLocation: 'src/tutorial/build/04-Props.js',
		exercises: [
			{
				tag: 'exercise1',
				location: 'src/exercise/04-Props.js',
				solutionLocation: 'src/exercise/solution/04-Props-solution.js'
			}
		]
	},
	{
		id: 'State',
		route: '/tutorial/understanding-state',
		displayName: '5 - 掌握 State',
		markdownLocation: 'src/tutorial/build/05-State.js',
		exercises: [
			{
				tag: 'exercise1',
				location: 'src/exercise/05-State.js',
				solutionLocation: 'src/exercise/solution/05-State-solution.js'
			}
		]
	},
	{
		id: 'LifecycleMethods',
		route: '/tutorial/lifecycle-methods',
		displayName: '6 - 生命周期函数',
		markdownLocation: 'src/tutorial/build/06-LifecycleMethods.js',
		exercises: [
			{
				tag: 'exercise1',
				location: 'src/exercise/06-LifecycleMethods.js',
				solutionLocation:
					'src/exercise/solution/06-LifecycleMethods-solution.js'
			}
		]
	},
	{
		id: 'HandlingEvents',
		route: '/tutorial/handling-events',
		displayName: '7 - 事件处理',
		markdownLocation: 'src/tutorial/build/07-HandlingEvents.js',
		exercises: [
			{
				tag: 'exercise1',
				location: 'src/exercise/07-HandlingEvents.js',
				solutionLocation: 'src/exercise/solution/07-HandlingEvents-solution.js'
			}
		]
	},
	{
		id: 'ComposingComponents',
		route: '/tutorial/composing-components',
		displayName: '8 - 组合组件',
		markdownLocation: 'src/tutorial/build/08-ComposingComponents.js',
		exercises: [
			{
				tag: 'exercise1',
				location: 'src/exercise/08-ComposingComponents.js',
				solutionLocation:
					'src/exercise/solution/08-ComposingComponents-solution.js'
			}
		]
	},
	{
		id: 'Capstone',
		route: '/tutorial/capstone',
		displayName: '9 - 进阶',
		markdownLocation: 'src/tutorial/build/09-Capstone.js',
		exercises: [
			{
				tag: 'exercise1',
				location: 'src/capstone/Capstone.js',
				solutionLocation: 'src/capstone/solution/Capstone.js'
			}
		]
	},
	{
		id: 'Conclusion',
		route: '/tutorial/conclusion',
		displayName: '10 - 未来的路',
		markdownLocation: 'src/tutorial/build/10-Conclusion.js'
	}
]
