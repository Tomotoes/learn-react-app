import React, { Component } from 'react'
import { TutorialMetadataApi } from './TutorialMetadataApi'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

const Container = styled.div`
	padding: 50px 10px 10px;
	text-align: center;
`

const Title = styled.div`
	font-size: 2rem;
	margin-bottom: 10px;
	padding: 15px;
`

const TutorialNavigation = styled.div`
	margin-top: 20px;
	margin-left: calc(40%);
	text-align: left;
`

const TutorialLink = styled.div`
	padding: 10px;
	font-size: 1.3rem;
	font-weight: bold;
`

class Home extends Component {
	constructor(props) {
		super(props)
		this.state = {
			tutorialMetadata: []
		}
	}

	componentDidMount() {
		this.setState({
			tutorialMetadata: TutorialMetadataApi.getTutorialMetadata()
		})
	}
	render() {
		const { tutorialMetadata } = this.state
		return (
			<Container>
				<Title>React 教程</Title>
				<div>
					<a href="https://github.com/tyroprogrammer/learn-react-app" target="_blank" rel="noopener noreferrer" >
						《React 教程》
					</a>
					是一本开源的 React 技术教程，适合初学者当作 React 入门教程。
				</div>
				<TutorialNavigation>
					{tutorialMetadata.map(({ id, route, displayName }) => {
						return (
							<TutorialLink key={id}>
								<Link to={route}>{displayName}</Link>
							</TutorialLink>
						)
					})}
				</TutorialNavigation>
			</Container>
		)
	}
}

export default Home
