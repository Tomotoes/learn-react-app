[React](https://reactjs.org) 是一个用于构建用户界面的声明式框架。 React 允许我们构建可重用的“组件” ，并组合这些组件来构建更加复杂的界面。 那就让我们从组件开始吧。

### 组件

组件是用户界面的组成部分——类似于 angular 中的指令，或者其他框架中的模块和小部件。React中的组件或多或少是自给自足的，因为它们构成了演示文稿( HTML )以及行为(例如事件处理程序)。它们也是可组合的——这意味着我们可以很容易地在其他组件中使用一个组件。那么我们如何创建一个组件呢？下面介绍几种可以创建 React 组件的常见方法。

#### 1. React.Component

创建 React 组件的一种方法是创建ES6类并继承 React.Component 。使用此方法创建的每个组件都应该有一个 render 函数，该函数返回实现该组件的 DOM 节点。

```jsx
import React from 'react';

class Component extends React.Component {
    //render function 是必须的
    render() {
			//如果此组件在浏览器中使用 ， 应该返回 该组件的 DOM 节点
        return (
             
        );
    }
}
```

#### 2. function
创建 React 组件的另一种常见方法是创建一个简单的函数，该函数接受一个参数(我们称为 `props` )，并返回与上面完全相同的内容。

```jsx
function Component(props) {
	//如果此组件在浏览器中使用 ， 应该返回 该组件的 DOM 节点
    return (
    );
}
```
*注意:用户定义的组件，必须[首字母大写](https://reactjs.org/docs/jsx-in-depth.html#user-defined-components-must-be-capitalized)。*

上面两种定义组件的方法，效果是相同的，但 React.Component可以做 一些函数不能做的事情，但我们先暂时暂停，我们将在本教程后，再回过头来看看。

让我们构建第一个 `HelloWorld` React 组件。
